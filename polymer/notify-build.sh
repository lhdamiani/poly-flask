#!/usr/bin/env bash




inotifywait -q -m -r -e modify $PWD'/../polymer/' |
while read f
    do
        polymer build;
        ./notify-build.sh;
        kill $PPID;
    done